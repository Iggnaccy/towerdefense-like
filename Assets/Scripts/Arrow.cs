﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class Arrow : MonoBehaviour
{
    [SerializeField] Rigidbody myRigidbody;

    private void Update()
    {
        transform.rotation = Quaternion.LookRotation(myRigidbody.velocity);
    }

    public void Initialize(Vector3 exitVelocity)
    {
        myRigidbody.velocity = exitVelocity;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Enemy"))
        {
            Destroy(other.gameObject);
        }
        Destroy(gameObject);
    }
}
