﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AFSInterview;

public class BurstTower : SimpleTower
{
    [SerializeField] float arrowMaxSpeed;
    [SerializeField] float burstShotDelay;

    protected override void Fire()
    {
        successfullyFired = false;

        StartCoroutine(FiringRoutine());

        if (successfullyFired)
            fireTimer = firingRate;
    }

    protected override void Shoot()
    {
        if (targetEnemy != null)
        {
            successfullyFired = true;
            Vector3 targetPosition = targetEnemy.transform.position;
            
            var targetVelocity = GetTargetVelocity(bulletSpawnPoint.position, targetPosition, targetEnemy.GetVelocity());

            var bullet = Instantiate(bulletPrefab, bulletSpawnPoint.position, Quaternion.identity).GetComponent<Arrow>();
            
            bullet.Initialize(targetVelocity);
        }
    }

    Vector3 GetTargetVelocity(Vector3 startPos, Vector3 currEnemyPos, Vector3 currEnemySpeed)
    {
        float dist = Mathf.Abs((startPos - currEnemyPos).magnitude);
        float time = dist / arrowMaxSpeed + 0.25f;
        Vector3 target = currEnemyPos + (currEnemySpeed * time);
        Vector3 velocity = target - startPos;
        dist = velocity.magnitude;
        velocity.y = 0;
        velocity = velocity.normalized * Mathf.Min(arrowMaxSpeed, dist * 2);
        return velocity;
    }

    IEnumerator FiringRoutine()
    {
        for(int i = 0; i < 3; i++)
        {
            Shoot();
            yield return new WaitForSeconds(burstShotDelay);
        }
    }
}
